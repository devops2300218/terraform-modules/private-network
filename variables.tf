variable "region" {
  description = "region of the network"
  type        = string
}

variable "network_name" {
  description = "name of the network"
  type        = string
}

variable "subnet_name" {
  description = "name of the subnet"
  type        = string
}

variable "cidr" {
  description = "cidr of the subnet"
  type        = string
}

variable "gateway_ip" {
  description = "gateway of the subnet"
  type        = string
}

variable "carp_port_name" {
  description = "name of the gateway carp port"
  type        = string
}

variable "primary_firewall_id" {
  description = "id of the primary firewall instance"
  type        = string
}

variable "primary_firewall_port_name" {
  description = "name of the primary firewall port"
  type        = string
}

variable "primary_firewall_ip" {
  description = "ip of the primary firewall"
  type        = string
}

variable "secondary_firewall_id" {
  description = "id of the secondary firewall instance"
  type        = string
}

variable "secondary_firewall_port_name" {
  description = "name of the secondary firewall port"
  type        = string
}

variable "secondary_firewall_ip" {
  description = "ip of the secondary firewall"
  type        = string
}

variable "routes" {
  description = "List of static routes to add"
  type = list(object({
    destination_cidr = string
    next_hop         = string
  }))
}

variable "dns_nameservers" {
  description = "String list with the DNS Server IP-Addresses"
  type        = list(string)
  default     = ["192.168.200.240", "8.8.8.8"]
}