resource "openstack_networking_network_v2" "network" {
  name           = var.network_name
  region         = var.region
  admin_state_up = true
}

resource "openstack_networking_subnet_v2" "subnet" {
  name            = var.subnet_name
  region          = var.region
  network_id      = openstack_networking_network_v2.network.id
  cidr            = var.cidr
  gateway_ip      = var.gateway_ip
  dns_nameservers = var.dns_nameservers
}

resource "openstack_networking_port_v2" "subnet_carp_port" {
  name                  = var.carp_port_name
  region                = var.region
  network_id            = openstack_networking_network_v2.network.id
  admin_state_up        = true
  port_security_enabled = false

  fixed_ip {
    subnet_id  = openstack_networking_subnet_v2.subnet.id
    ip_address = var.gateway_ip
  }
}

resource "openstack_networking_port_v2" "subnet_primary_firewall_port" {
  name                  = var.primary_firewall_port_name
    region              = var.region
  network_id            = openstack_networking_network_v2.network.id
  admin_state_up        = true
  port_security_enabled = false

  fixed_ip {
    subnet_id  = openstack_networking_subnet_v2.subnet.id
    ip_address = var.primary_firewall_ip
  }
}

resource "openstack_compute_interface_attach_v2" "subnet_primary_firewall_port_attach" {
  instance_id = var.primary_firewall_id
  region      = var.region
  port_id     = openstack_networking_port_v2.subnet_primary_firewall_port.id
}

resource "openstack_networking_port_v2" "subnet_secondary_firewall_port" {
  name                  = var.secondary_firewall_port_name
  region                = var.region
  network_id            = openstack_networking_network_v2.network.id
  admin_state_up        = true
  port_security_enabled = false

  fixed_ip {
    subnet_id  = openstack_networking_subnet_v2.subnet.id
    ip_address = var.secondary_firewall_ip
  }
}

resource "openstack_compute_interface_attach_v2" "subnet_secondary_firewall_port_attach" {
  instance_id = var.secondary_firewall_id
  region      = var.region
  port_id     = openstack_networking_port_v2.subnet_secondary_firewall_port.id
}

resource "openstack_networking_subnet_route_v2" "subnet_custom_route" {
  count = length(var.routes)

  subnet_id        = openstack_networking_subnet_v2.subnet.id
  region           = var.region
  destination_cidr = var.routes[count.index].destination_cidr
  next_hop         = var.routes[count.index].next_hop
}
