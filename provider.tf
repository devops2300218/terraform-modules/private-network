terraform {
  required_version = "~> 1.5"
  required_providers {

    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.51.1"
    }
    ovh = {
      source  = "ovh/ovh"
      version = "~> 0.34.0"
    }
  }
}
